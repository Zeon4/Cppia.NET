using Cppia.Instructions;
using Cppia.Runtime;

namespace Cppia;

public class Log
{
    public static Action<object, PosInfoInstruction> Trace { get; set; } = DefaultTrace;

    public static void DefaultTrace(object message, PosInfoInstruction posInfo) 
        => Console.WriteLine($"{posInfo.File}:{posInfo.Line}: {message}");
}