namespace Cppia.Instructions;

public class AndInstruction : BinOperationInstruction
{
    public AndInstruction(CppiaFile file, CppiaReader reader) 
        : base(file, reader) {}
}