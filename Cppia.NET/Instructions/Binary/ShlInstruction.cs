namespace Cppia.Instructions;

public class ShlInstruction : BinOperationInstruction
{
    public ShlInstruction(CppiaFile file, CppiaReader reader) 
        : base(file, reader) {}
}