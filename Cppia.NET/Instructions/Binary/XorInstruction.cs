namespace Cppia.Instructions;

public class XorInstruction : BinOperationInstruction
{
    public XorInstruction(CppiaFile file, CppiaReader reader) 
        : base(file, reader) {}
}