namespace Cppia.Instructions;

public class IntervalInstruction : BinOperationInstruction
{
    public IntervalInstruction(CppiaFile file, CppiaReader reader) 
        : base(file, reader) {}
}