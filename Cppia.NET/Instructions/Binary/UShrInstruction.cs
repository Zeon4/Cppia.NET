namespace Cppia.Instructions;

public class UShrInstruction : BinOperationInstruction
{
    public UShrInstruction(CppiaFile file, CppiaReader reader) 
        : base(file, reader) {}
}