namespace Cppia.Instructions;

public class AssignOrInstruction : SetInstruction
{
    public AssignOrInstruction(CppiaFile file, CppiaReader reader) 
        : base(file, reader) {}
}