namespace Cppia.Instructions;

public class AssignShrInstruction : SetInstruction
{
    public AssignShrInstruction(CppiaFile file, CppiaReader reader) 
        : base(file, reader) {}
}