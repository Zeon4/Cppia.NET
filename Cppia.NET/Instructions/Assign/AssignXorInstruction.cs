namespace Cppia.Instructions;

public class AssignXorInstruction : SetInstruction
{
    public AssignXorInstruction(CppiaFile file, CppiaReader reader) 
        : base(file, reader) {}
}