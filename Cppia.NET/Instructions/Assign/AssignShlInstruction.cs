namespace Cppia.Instructions;

public class AssignShlInstruction : SetInstruction
{
    public AssignShlInstruction(CppiaFile file, CppiaReader reader) 
        : base(file, reader) {}
}