using System.Reflection;
using Cppia.Runtime;

namespace Cppia.Instructions;

public class CallInstruction : BaseCallInstruction
{
    public CppiaInstruction Function { get; }

    public CallInstruction(CppiaFile file, CppiaReader reader)
    {
        int argsCount = reader.ReadByte();
        Function = ReadInstruction(file,reader);
        ReadArguments(file, reader, argsCount);
    }

    public override object? Execute(Context context)
    {
        object? instance = (Function as BaseFieldInstruction)?.Object;
        var args = GetArguments(context);
        var value = Function.Execute(context);
        if (value is IMethod method)
            return method.Invoke(instance, args);
        else if(value is Delegate function)
            return function.DynamicInvoke(args);
        throw new NotImplementedException();
    }
}