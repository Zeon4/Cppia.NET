namespace Cppia.Instructions;

public class ConstThisInstruction : BaseConstInstruction
{
    public ConstThisInstruction(CppiaFile file, CppiaReader reader) 
    : base(file, reader){}
}