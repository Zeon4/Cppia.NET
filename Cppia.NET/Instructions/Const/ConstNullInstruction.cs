namespace Cppia.Instructions;

public class ConstNullInstruction : BaseConstInstruction
{
    public ConstNullInstruction(CppiaFile file, CppiaReader reader) 
    : base(file, reader){}
}